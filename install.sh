# execute this script via: bash <(curl -Ls https://goo.gl/QwGNwg)

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
error () {
  echo "`tput setaf 1`$1`tput op;`"
}

info () {
  echo "`tput setaf 6`$1`tput op;`"
}

info "Start cloning bitvoodoo-sdk..."
git clone git@bitbucket.org:bitvoodoo/bitvoodoo-sdk.git

if [ -d "${SCRIPT_DIR}/bitvoodoo-sdk" ]; then
	cd bitvoodoo-sdk/
	git checkout --track origin/feature/BVSDK-68-python3-port
	./init.sh
	exit 0
else
	error "${SCRIPT_DIR} does not exist, therefore something went wrong during the cloning."
	info "Validate your SSH key on your computer and on your Bitbucket account."
	exit 1
fi
